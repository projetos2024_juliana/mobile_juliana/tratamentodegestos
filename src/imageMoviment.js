import React, { useState, useEffect, useRef } from "react";
import { View, Dimensions, PanResponder, Image, FlatList, StyleSheet } from "react-native";

const ImageMoviment = () => {
  const [imageUrls, setImageUrls] = useState([]);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx < -gestureThreshold) {
          showNextImage();
        } else if (gestureState.dx > gestureThreshold) {
            showNextImage();
        }
      },
    })
  ).current;

  const fetchImages = () => {
    const accessKey = "F9jr_3oPgdpr5t6xa1lsh7zBQ02RgDAKSTRycRRI5YE";
    const apiUrl = `https://api.unsplash.com/photos/random?count=3&client_id=${accessKey}`;

    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        const urls = data.map((photo) => photo.urls.regular);
        setImageUrls(urls);
      })
      .catch((error) => console.error("Erro ao buscar imagens:", error));
  };

  useEffect(() => {
    fetchImages();
  }, []);

  const showNextImage = () => {
    if (currentImageIndex < imageUrls.length - 1) {
      setCurrentImageIndex((prevIndex) => prevIndex + 1);
    } else {
      fetchImages(); // Se não houver mais imagens, recarrega
      setCurrentImageIndex(0);
    }
  };

  return (
    <View {...panResponder.panHandlers} style={styles.container}>
      <FlatList
        horizontal
        pagingEnabled
        data={[imageUrls[currentImageIndex]]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <Image source={{ uri: item }} style={styles.image} />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: 200,
    height: 200,
    alignSelf: 'center', 
  },
});

export default ImageMoviment;
