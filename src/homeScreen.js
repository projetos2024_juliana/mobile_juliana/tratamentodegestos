import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("CountMoviment")}>
        <Text style={styles.buttonText}>Ir para Contagem</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("ImageMoviment")}>
        <Text style={styles.buttonText}>Rolagem de Imagem</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    backgroundColor: "#3498db", 
    padding: 10,
    margin: 5,
    borderRadius: 5,
  },
  buttonText: {
    color: "#ffffff", 
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default HomeScreen;
