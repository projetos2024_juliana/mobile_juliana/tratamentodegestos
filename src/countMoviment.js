import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Button, Text, TouchableOpacity } from "react-native";

const CountMoviment = ({ navigation }) => {
  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshold = screenHeight * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshold) {
          setCount((prevCount) => prevCount + 1);
        }
      },
    })
  ).current;

  return (
    <View {...panResponder.panHandlers} style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Valor do Contador: {count}</Text>
      <Button title="Voltar para Home" onPress={() => navigation.navigate("Home")} />
    </View>
  );
};

export default CountMoviment;