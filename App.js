import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import HomeScreen from "./src/homeScreen";
import ImageMoviment from "./src/imageMoviment";

const Stack = createStackNavigator();

export function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="CountMoviment" component={CountMoviment} />
        <Stack.Screen name="ImageMoviment" component={ImageMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
